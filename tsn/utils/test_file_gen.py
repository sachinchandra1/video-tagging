import os 
import cv2
import argparse


def test_file_generator(video_path, file_path):
    f = open(file_path, 'w')
    for dirs, subdirs, files in os.walk(args.path):
        files.sort()
        for file in files:
            file_path = os.path.join(dirs, file)
            print(file_path)
            out_path = os.path.join(dirs.replace('videos', 'ops'), file.split('.')[0])
            video = cv2.VideoCapture(file_path)
            print(video.get(cv2.CAP_PROP_FPS))
            num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT)) - 20
            line = '{} {}'.format(out_path, num_frames)
            f.write(line + '\n')
    f.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Argument Parser')
    parser.add_argument('--path', type=str)
    parser.add_argument('--file_path', type=str, default='test_video_list.txt')
    args = parser.parse_args()

    test_file_generator(args.path, args.file_path)