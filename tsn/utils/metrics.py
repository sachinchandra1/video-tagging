"""
This module provides some utils for calculating metrics
"""
import numpy as np
from sklearn.metrics import average_precision_score, confusion_matrix
import pandas as pd
import argparse


def softmax(raw_score, T=1):
    exp_s = np.exp((raw_score - raw_score.max(axis=-1)[..., None])*T)
    sum_s = exp_s.sum(axis=-1)
    return exp_s / sum_s[..., None]


def top_k_acc(lb_set, scores, k=3):
    idx = np.argsort(scores)[-k:]
    return len(lb_set.intersection(idx)), len(lb_set)


def top_k_hit(lb_set, scores, k=3):
    idx = np.argsort(scores)[-k:]
    return len(lb_set.intersection(idx)) > 0, 1


def top_3_accuracy(score_dict, video_list):
    return top_k_accuracy(score_dict, video_list, 3)


def top_k_accuracy(score_dict, video_list, k):
    video_labels = [set([i.num_label for i in v.instances]) for v in video_list]

    video_top_k_acc = np.array(
        [top_k_hit(lb, score_dict[v.id], k=k) for v, lb in zip(video_list, video_labels)
         if v.id in score_dict])

    tmp = video_top_k_acc.sum(axis=0).astype(float)
    top_k_acc = tmp[0] / tmp[1]

    return top_k_acc


def video_mean_ap(score_dict, video_list):
    avail_video_labels = [set([i.num_label for i in v.instances]) for v in video_list if
                          v.id in score_dict]
    pred_array = np.array([score_dict[v.id] for v in video_list if v.id in score_dict])
    gt_array = np.zeros(pred_array.shape)

    for i in xrange(pred_array.shape[0]):
        gt_array[i, list(avail_video_labels[i])] = 1
    mean_ap = average_precision_score(gt_array, pred_array, average='macro')
    return mean_ap


def mean_class_accuracy(scores, labels):
    pred = np.argmax(scores, axis=1)
    cf = confusion_matrix(labels, pred).astype(float)

    cls_cnt = cf.sum(axis=1)
    cls_hit = np.diag(cf)

    return np.mean(cls_hit/cls_cnt)

def union(num1, num2):
    num1 = list(range(num1[0], num1[1]))
    num2  = list(range(num2[0], num2[1]))
    return len(list(set(num1) & set(num2)))


def map_score(actual_loc, predicted_loc, labels):
    """ Calculates map scores for individual classes
    actual_loc: pandas dataframe location with columns ["s no", "Video_id", "category", "start_time", "end_time"]
    predicted_loc: pandas dataframe location with columns ["s no", "Video_id", "category", "start_time", "end_time"]

    """
    actual = pd.read_csv(actual_loc)
    predicted = pd.read_csv(predicted_loc)
    unique_videos_actual = list(actual["Video_id"].unique())
    unique_videos_predicted = list(actual["Video_id"].unique())
    is_all_present = [i for i in unique_videos_predicted if i not in unique_videos_actual]
    if len(is_all_present) > 0:
        print("Some Videos are not present.. Raising errors")
        return None
    # is_all_labels_present_actual = [i for i in list(actual["category"].unique()) if i not in labels]
    # is_all_labels_present_predicted = [i for i in list(predicted["category"].unique()) if i not in labels]
    # if (is_all_labels_present_predicted >0) or (is_all_labels_present_actual>0):
    #     print("Labels miss-match. Check your dataframe category and the labels list provided to the function")
    #     return None
    metric = {}
    for i in unique_videos_actual:
        if i not in metric.keys():
            metric[i] = {}
        o = actual[actual["Video_id"] == i]
        t = predicted[predicted["Video_id"] == i]
        cats = list(o["category"].unique())
        for cat in cats:
            if cat not in metric[i].keys():
                metric[i][cat] = 0
            c = t[t["category"] == cat]
            c_p = o[o["category"] == cat]
            if len(c) == 0:
                continue
            elif len(c_p) == 0:
                continue
            else:
                c["total_time"] = c["end_time"] - c["start_time"]
                c_p["total_time"] = c_p["end_time"] - c_p["start_time"]
                total_time = float(np.sum(c["total_time"].values))
                total_time_predicted = float(np.sum(c_p["total_time"].values))
                actual_action_timing = c[["start_time", "end_time"]].values
                predicted_action_timing = c_p[["start_time", "end_time"]].values
                intersection = []
                for aat in actual_action_timing:
                    for pat in predicted_action_timing:
                        score = union(aat, pat)
                        intersection.append(score)
                total_intersection = np.sum(intersection)
                metric[i][cat] = total_intersection/(total_time+total_time_predicted-total_intersection)


    la = {key: [] for key in labels}
    for k in metric.keys():
        me = metric[k]
        for j in me.keys():
            la[j].append(me[j])

    return la


def scores(rm):
    mm = []
    for i in rm:
        k = range(i[0], i[1])
        dd = [ll for ll in k if ll%5==0]
        los2 = [dd[i] for i in range(len(dd))]
        mm.extend(los2)
    return mm


def hit_metric(actual_loc, predicted_loc, labels):
    """
    we take each Video_id  from test set and loop:
        we see which labels are in test
            if a label is not present
                but present in predicted file we make it zero
                not present in predicted file we make it None

            if a label is present:
                if label is not present in the predicted file we make it zero
                if label is present in the predicted file we calculate the metric
    """
    actual = pd.read_csv(actual_loc)
    predicted = pd.read_csv(predicted_loc)
    unique_videos_actual = list(actual["Video_id"].unique())
    unique_videos_predicted = list(actual["Video_id"].unique())
    is_all_present = [i for i in unique_videos_predicted if i not in unique_videos_actual]
    if len(is_all_present) > 0:
        print("Some Videos are not present.. Raising errors")
    metric = {}
    for i in unique_videos_actual:
        if i not in metric.keys():
            metric[i] = {}
        a = actual[actual["Video_id"] == i]
        p = predicted[predicted["Video_id"] == i]
        okk = {}
        for la in labels:
            if la not in okk.keys():
                okk[la] = None
            cat_a = a[a["category"] == la]
            cat_p = p[p["category"] == la]
            if (len(cat_a) ==0):
                if (len(cat_p)==0):
                    continue
                else:
                    okk[la] =0
            else:
                if len(cat_p) == 0:
                    okk[la] = 0
                else:
                    mm = list(a[["start_time", "end_time"]].values)
                    mm2 = list(p[["start_time", "end_time"]].values)
                    mm = scores(mm)
                    mm2 = scores(mm2)
                    hit_rate = len(list(set(mm) & set(mm2)))
                    okk[la] = hit_rate/len(mm)
        metric[i] = okk
    name = {key:[] for key in labels}

    for k in metric.keys():
        m = metric[k]
        for ki in m.keys():
            if m[ki] is not None:
                name[ki].append(m[ki])
    return name

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='File inputs')
    parser.add_argument('--original_file', type=str, default=None)
    parser.add_argument('--test_file', type=str, default=None) 
    args = parser.parse_args()
    labels = ['dish_washing', 'vacuuming', 'mopping_floor', 'sweeping_floor', 'other', 'cooking', 'eating']
    output = map_score(args.original_file, args.test_file, labels)
    list1 = output['dish_washing']
    print(np.mean(list1))
    #print(output)
    output_hitrate = hit_metric(args.original_file, args.test_file, labels)
    list12 = output_hitrate['sweeping_floor']
    print(np.mean(list12))
    print(output_hitrate)