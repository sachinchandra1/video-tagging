import os
import argparse
import cv2


def train_val_generator(folder_path, optical_flow_path):
    train = open('train.txt', 'w')
    val = open('val.txt', 'w')
    for dirs, subdirs, files in os.walk(folder_path):
        for j, d in enumerate(subdirs):
            files = os.listdir(os.path.join(dirs, d))
            for i, f in enumerate(files):
                filename = os.path.join(dirs, d, f)
                print(filename)
                filename_processed = os.path.join(optical_flow_path, f)
                video = cv2.VideoCapture(filename)
                num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT)) - 20
                line = '{} {} {}\n'.format(filename_processed.split('.')[0], num_frames, j)
                if i < 0.75*(len(files)):
                    train.write(line)
                else:
                    val.write(line)

if __name__ == '__main__':
    argument_parser = argparse.ArgumentParser(description="path variable")
    argument_parser.add_argument('--videos_path', type=str, help='path to videos')
    argument_parser.add_argument('--optical_flow_path', type=str, help='path to optical flows of videos')
    args = argument_parser.parse_args()
    
    train_val_generator(args.videos_path, args.optical_flow_path)