import os
import pandas as pd
import argparse


def merge_csvs(folder_name, final_csv_name):
    file_list = []
    for dirs, subdirs, files in os.walk(folder_name):
        for f in files:
            if len(f.split('.')) > 1:
                if f.split('.')[1] == 'csv':
                    file_list.append(os.path.join(dirs, f))

    combined_csv = pd.concat([pd.read_csv(f) for f in file_list])

    combined_csv.to_csv(final_csv_name, index=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(decsription='input params')
    parser.add_argument('--folder_name', type='str', help='folder with csvs to combine')
    parser.add_argument('--final_csv', type='str', help='name of the final combined csv')

    args = parser.parse_args()
    merge_csvs(args.folder_name, args.final_csv)