import os
import argparse
import glob
import time


def run_model_on_folder(folder_path, result_path, split, flow_model, rgb_model, is_pretrained_model, num_classes):
    if not os.path.exists(result_path):
        os.makedirs(result_path)
    start = time.time()

    for dirs, subdirs, files in os.walk(folder_path):
        for filename in files:    
            print("Testing on Video {}".format(filename))
            # copy video files to testing folder environment
            os.system("cp {} test_dataset/videos/".format(os.path.join(folder_path+filename)))
            # split video into given time chunks
            os.system("python utils/video_splitter.py -f {} -s {} -v h264".format('test_dataset/videos/'+filename, split))
            # remove video from testing environment folder as we won't need optical flows for whole videos
            os.system("rm test_dataset/videos/{}".format(filename))
            # generate the test files to process to pass through test code
            os.system("python utils/test_file_gen.py --path test_dataset/videos")
            # build optical flows for the split video chunks
            os.system("python utils/build_of.py test_dataset/videos/ test_dataset/ops/ --ext mp4")

            filename = filename.split('.')[0]
            # run the split videos through flow model
            os.system("python inference.py ucf101 Flow test_video_list.txt {} --arch BNInception --num_class {} --save_scores {}/flow-{} --pretrained_model {} --flow_pref flow_ -j 2".format(flow_model, num_classes, result_path, filename, is_pretrained_model))
            # run the split videos through rgb model
            os.system("python inference.py ucf101 RGB test_video_list.txt {} --arch BNInception --num_class {} --save_scores {}/rgb-{} --pretrained_model {} -j 2".format(rgb_model, num_classes, result_path, filename, is_pretrained_model))
            # merge the scores from both rgb and flow model
            os.system("python test.py {}/rgb-{}.npz {}/flow-{}.npz --save_file {} --score_weights 1 2 --split_time {} --result_folder {} > {}/pred-{}".format(result_path, filename, result_path, filename, filename, split, result_path, result_path, filename))
            # remove the processed video from the testing folder environment
            os.system("rm -r test_dataset/videos/*")
            # os.system("rm -r {}*.npy".format(result_path))
            # combine all the resulting csvs
            os.system("python generate_final_csv.py --folder_name {} --final_csv final_results.csv".format(result_path))

    print('inference complete in ' + str(time.time() - start))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="single script to run inference on video folder")
    parser.add_argument('--path', type=str, help="path to video folder")
    parser.add_argument('--size', type=int, help="chunk sizes", default=10)
    parser.add_argument('--rgb_model', type=str, default='rgb.pth')
    parser.add_argument('--flow_model', type=str, default='flow.pth')
    parser.add_argument('--num_classes', type=str, default=4)
    parser.add_argument('--result_path', type=str)
    parser.add_argument('--pretrained_model', type=str, default='no')
    args = parser.parse_args()

    run_model_on_folder(args.path, args.result_path, args.size, args.flow_model, args.rgb_model, args.pretrained_model, args.num_classes)