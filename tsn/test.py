"""
Code to merge the results of different modalities and come up with final
prediction
"""

import argparse
import numpy as np
import pandas as pd

from utils.metrics import softmax
from utils.video_utils import default_aggregation_func
from utils.metrics import mean_class_accuracy


parser = argparse.ArgumentParser()
parser.add_argument('score_files', nargs='+', type=str)
parser.add_argument('--save_file', type=str, default="results.txt")
parser.add_argument('--score_weights', nargs='+', type=float, default=None)
parser.add_argument('--split_time', type=int, default=5)
parser.add_argument('--result_folder', type=str)
parser.add_argument('--result_file', type=str)
parser.add_argument('--crop_agg', type=str, choices=['max', 'mean'], default='mean')
parser.add_argument('--split_videos', type=str, default='no')
args = parser.parse_args()

save_file = args.save_file
score_npz_files = [np.load(x) for x in args.score_files]
split_time = args.split_time
if args.score_weights is None:
    score_weights = [1] * len(score_npz_files)
else:
    score_weights = args.score_weights
    if len(score_weights) != len(score_npz_files):
        raise ValueError("Only {} weight specifed for a total of {} score files"
                         .format(len(score_weights), len(score_npz_files)))

score_list = [x['scores'][:, 0] for x in score_npz_files]
label_list = [x['labels'] for x in score_npz_files]
# label verification

# score_aggregation
agg_score_list = []
for score_vec in score_list:
    agg_score_vec = [default_aggregation_func(x, normalization=False, crop_agg=getattr(np, args.crop_agg)) for x in score_vec]
    agg_score_list.append(np.array(agg_score_vec))

final_scores = np.zeros_like(agg_score_list[0])
for i, agg_score in enumerate(agg_score_list):
    final_scores += agg_score * score_weights[i]

# accuracy
prob_list = [softmax(x) for x in final_scores]
class_list_new = []
df = pd.read_csv('/home/sachin/Downloads/kinetics_train/kinetics_train.csv')
class_list_names = list(set(df['label']))
class_list_names.sort()
pg_class_labels = ['cleaning floor', 'cooking chicken', 'cooking egg', 'cooking sausages',
					'cooking on campfire', 'eating burger', 'eating carrots', 'eating chips',
					'eating doughnuts', 'eating hotdog', 'eating spaghetti', 'eating icecream',
					'eating watermelon', 'making a cake', 'making a sandwich', 'making bed',
					'making tea', 'mopping floor', 'washing dishes', 'washing hands', 'building cabinet',
					'cutting watermelon', 'carving pumpkin', 'cutting pineapple', 'frying vegetables',
					'grinding meat', 'making pizza', 'making sushi', 'peeling apples', 'peeling potatoes',
					'scrambling eggs', 'sweeping floor', 'barbequing', 'tossing salad', 'baking cookies', 'moving furniture']
new_class_labels = ['other' if label not in pg_class_labels else label for label in class_list_names]

class_labels_4class = ['dish_washing', 'sweeping_floor', 'cooking', 'eating', 'vacuuming']

class_list = np.argmax(final_scores, axis=1)

roc_curve_list = []

for i, val in enumerate(prob_list):
    val_final = np.zeros(np.shape(val))
    val_final = (val > 0.8).astype(np.uint8)
   
    # Probability cutoffs for pg_classes 

    # val_final[0] = (val[0] > 0.8).astype(np.uint8)
    # val_final[1] = (val[1] > 0.8).astype(np.uint8)
    # val_final[2] = (val[2] > 0.8).astype(np.uint8)
    # val_final[3] = (val[3] > 0.8).astype(np.uint8)
    # val_final[4] = (val[4] > 0.8).astype(np.uint8)
    
    if not np.all(val == 0):
        class_list_new.append(class_list_names[np.argmax(np.array(val))])
    else:
        class_list_new.append('other')

if args.split_videos == 'yes':
    f = open('test_video_list.txt', 'r')
    f_list = []

    for line in f.readlines():
        f_list.append(line.split(' ')[0].split('/')[-1])

    result_dict_single = []
    for i, l in enumerate(class_list_new):
        result_dict_single.append((f_list[i], l, np.amax(prob_list[i])))

    dict_single = pd.DataFrame(result_dict_single)
    dict_single.columns = ["Video_id", "class", "probabilities"]
    dict_single.to_csv(args.result_file, index=False)

else:
    cat = class_list_new[0]
    start_index = 0
    count = 0
    result_dict = []  
    for i, l in enumerate(class_list_new):
        if l == cat:
            count = count + 1
        else:
            result_dict.append((save_file+'.mp4', cat, start_index*split_time,
                                (start_index+count)*split_time))
            start_index = i
            cat = l
            count = 1
    result_dict.append((save_file+'.mp4', cat, start_index*split_time,
                        (start_index+count)*split_time))

    dicts = pd.DataFrame(result_dict)
    dicts.columns = ["Video_id", "category", "start_time", "end_time"]
    dicts.to_csv('{}/'.format(args.result_folder) + save_file+".csv", index=False)


for l in prob_list:
    l = l.tolist()
    l2 = [l.index(x) for x in sorted(l)]
    l.sort()
    kkk = []
    for i, m in enumerate(l2[395:]):
        kkk.append((class_list_names[m], l[i+395]))
    # for i, m in enumerate(l2):
    #   kkk.append((class_labels_4class[m], l[i]))
    print(kkk)
    del kkk[:]
acc = mean_class_accuracy(final_scores, label_list[0])
# print ('Final accuracy {:02f}%'.format(acc * 100))
